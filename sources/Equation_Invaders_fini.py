import pygame
from pygame.locals import *
from random import randint, shuffle
from sympy.solvers import solve

def invaders():
    '''Ecran de début de jeu'''

    pygame.init()

    screen = pygame.display.set_mode((1280, 960))
    pygame.display.set_caption("Equation Invaders")
    background = pygame.image.load('Ressources\Background.jpg')
    police = pygame.font.SysFont("Arial" ,80)
    police_consigne = pygame.font.SysFont("Arial" ,40)

    start_init_game = pygame.time.get_ticks()

    deb = True
    run = True
    ecran_fin = True

    while deb:
        
        sec = (pygame.time.get_ticks()-start_init_game)/1000
        time_left_deb = 8 - int(sec)
        if time_left_deb == 0:
            deb = False
            
        phr_deb = police.render('Bienvenue sur Equation Invaders !', 1 , (233,56,63))
        consigne_1 = police_consigne.render('Utiliser les flèches pour aller à doite et à gauche', 1, (255, 111, 125))
        consigne_2 = police_consigne.render('Utiliser la barre espace pour tirer sur le vaisseau avec la bonne solution', 1, (255, 111, 125))
        consigne_3 = police_consigne.render("Le but est de gagner le plus de points en résolvant le plus d'équation", 1, (255, 111, 125))
        
        screen.fill((0, 0, 0))
        screen.blit(background, (0,0))
        screen.blit(phr_deb, (150, 200))
        screen.blit(consigne_1, (300, 600))
        screen.blit(consigne_2, (125, 650))
        screen.blit(consigne_3, (150, 700))
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                deb = False
                run = False
                ecran_fin = False
        
        pygame.display.update()

    pygame.quit()

    '''Ecran principal du jeu'''

    pygame.init()

    screen = pygame.display.set_mode((1280, 960))
    pygame.display.set_caption("Equation Invaders")
    background = pygame.image.load('Ressources\Background.jpg')
    vaisseaux = pygame.image.load('Ressources\Vaisseau.png')
    rayon = pygame.image.load('Ressources\Rayon_Laser.png')

    aliens_real = pygame.image.load('Ressources\Vaisseau_Aliens.png')
    aliens_fake1 = pygame.image.load('Ressources\Vaisseau_Aliens.png')
    aliens_fake2 = pygame.image.load('Ressources\Vaisseau_Aliens.png')
    aliens_fake3 = pygame.image.load('Ressources\Vaisseau_Aliens.png')

    police = pygame.font.SysFont("Arial" ,50)
    police_sol = pygame.font.SysFont("Helvetica" ,30)

    start = pygame.time.get_ticks()
    life = False
    pt = 0

    #Créer les équations:
    def equation():
        a = randint(-10, 10)
        b = randint(-10, 10)
        while a == 0 or a == b or a == -b:
            a = randint(-10, 10)
        while b == 0:
            b = randint(-10, 10)
        if b < 0:
            equ = f'{a}*x{b}'
        else:
            equ = f'{a}*x+{b}'
        return equ

    #Les résoudre:
    def reponse(equ):
        return solve(equ, 'x')[0]

    #Lancement du programme
    while run:
        
        sec = (pygame.time.get_ticks()-start)/1000
        time_left = 60 - int(sec)
        if time_left == 0:
            run = False
            
        time = police.render(f'Temps restant : {time_left}s', 1 , (200,10,0))
        
        if life == False:
        
            #Création de l'équation et ses solutions
            equa = equation()
            sol_real = reponse(equa)
            sol_fake1 = 1/sol_real
            sol_fake2 = -sol_real
            sol_fake3 = -sol_fake1

            #Afficher les équations et réponses:
            equ_affichage = equa.replace('*', '')
            equ = f'Résoudre : {equ_affichage} = 0'
            quest = police.render(equ, 1 , (200,10,0))

            life = True

            x_laser, y_laser = -500, -500 
            x, y = 391, 550
            move_x, move_y = 0, 0

            pos = [45, 355, 665, 975]	#Déterminer la position des réponses
            shuffle(pos)
            coord_laser = []

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                ecran_fin = False
            
            if life == True:
                
                #Effectuer des actions en fonctions de la touche appuyée
                if event.type == KEYDOWN:
                    if event.key == K_LEFT: #Déplacement du vaisseaux
                        move_x = -40
                    elif event.key == K_RIGHT:
                        move_x = +40
                    if event.key == K_SPACE: #Tir du rayon laser
                        x_laser = x + 225
                        y_laser = y
                        while y_laser > -283:
                            screen.blit(rayon, (x_laser, y_laser))
                            screen.blit(vaisseaux, (x, y))
                            pygame.display.update()
                            y_laser -= 10
                        coord_laser = [(x+225, y) for y in range(y_laser, y_laser+500)]
                elif event.type == KEYUP: #Arret du déplacement
                    if event.key == K_LEFT:
                        move_x = 0
                    elif event.key == K_RIGHT:
                        move_x = 0
                        
            x += move_x
            y += move_y
            
            point = police.render(f'Points : {pt}', 1 , (200,10,0))
            
            screen.fill((0, 0, 0))	#Affichage du fond d'écran et du nb de pts et le tps restant
            screen.blit(background, (0,0))
            screen.blit(point, (1070,15))
            screen.blit(time, (550, 15))
            
            coord_sol_real = [(z, 75) for z in range(pos[0], pos[0]+200)]
            for elmt in coord_laser:
                if elmt in coord_sol_real:
                    life = False
                    pt += 1
                coord_laser = []
                
            if life == True:	#Affichage des aliens quand la réponse n'est pas encore trouvée
                screen.blit(vaisseaux, (x, y))
                screen.blit(quest, (40,15))
                screen.blit(aliens_real, (pos[0], 75))
                screen.blit(aliens_fake1, (pos[1], 75))
                screen.blit(aliens_fake2, (pos[2], 75))
                screen.blit(aliens_fake3, (pos[3], 75))
                #Affiche des solutions sur les vaisseaux correspondant
                screen.blit(police_sol.render(str(sol_real), 1 , (0,150,0)), (pos[0]+117,138))
                screen.blit(police_sol.render(str(sol_fake1), 1 , (0,150,0)), (pos[1]+117,138))
                screen.blit(police_sol.render(str(sol_fake2), 1 , (0,150,0)), (pos[2]+117,138))
                screen.blit(police_sol.render(str(sol_fake3), 1 , (0,150,0)), (pos[3]+117,138))
            
        pygame.display.update()
            
    pygame.quit()

    '''Ecran de fin de jeu'''

    pygame.init()

    screen = pygame.display.set_mode((1280, 960))
    pygame.display.set_caption("Equation Invaders")
    background = pygame.image.load('Ressources\Background.jpg')
    police = pygame.font.SysFont("Arial", 70)

    while ecran_fin:
        
        phr_fin = police.render(f'Vous avez fini avec {pt} points', 1 , (233,56,63))
        
        screen.fill((0, 0, 0))
        screen.blit(background, (0,0))
        screen.blit(phr_fin, (270, 400))
            
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                ecran_fin = False
                
        pygame.display.update()        
            
    pygame.quit()