def pythagore():
    import pygame
    import random
    import sys
    import time
    
    pygame.init()

    # Définir la taille de l'écran d'acceuil
    largeur, hauteur = 800, 600
    taille_ecran = (largeur, hauteur)

    #musique d'ambiance
    def jouer_musique(musique):
        pygame.mixer.init()
        pygame.mixer.music.load(musique)
        pygame.mixer.music.play(-1)
        
    chemin_musique = "Ressources\C.mp3"
    jouer_musique(chemin_musique)
    #le time sleep correspond au debbut de la musique 
    time.sleep(4)

    #créer l'écran
    ecran = pygame.display.set_mode(taille_ecran)
    pygame.display.set_caption("Écran d'Accueil")

    #couleurs
    blanc = (255, 255, 255)
    noir = (0, 0, 0)
    bleu = (89, 255, 6 )
    rouge = (255, 0, 0)

    #police
    police = pygame.font.Font("Ressources\police.ttf", 30)

    #texte
    texte_accueil = police.render("Bienvenue sur l'écran d'accueil", True, blanc)
    texte_instructions = police.render("Cliquez sur le bouton pour commencer", True, blanc)

    #position du texte
    position_texte_accueil = texte_accueil.get_rect(center=(largeur // 2, hauteur // 2 - 50))
    position_texte_instructions = texte_instructions.get_rect(center=(largeur // 2, hauteur // 2 + 50))

    #bouton start
    bouton_rect = pygame.Rect(largeur // 2 - 100, hauteur // 2 + 100, 200, 50)
    texte_bouton = police.render("Start", True, noir)
    position_texte_bouton = texte_bouton.get_rect(center=bouton_rect.center)

    def ecran_titre(noir,blanc,police,pts_finaux):
        ecran = pygame.display.set_mode((680,630))
        fond4  = pygame.image.load("Ressources\9.png").convert()
        pygame.display.set_caption("ecran titre")
        
        textes = [{"texte": "Merci d'avoir joué", "y": 630 // 2 - 250},
                 {"texte": f"Points gagnés : {pts_finaux}", "y": 630 // 2 - 200},
                 {"texte": "Vous avez terminé le jeu.", "y": 630 // 2 -150},
                 {"texte": "MATHSLEARNING", "y": 630 // 2-75 }]
        #pos initiale du texte
        x = 680 // 2
        clock = pygame.time.Clock()

        vrai = True

        while vrai:
            ecran.blit(fond4,(0,0))

            for texte_info in textes:
                #affiche le nv texte
                texte = police.render(texte_info["texte"], True, blanc)
                texte_rect = texte.get_rect(center=(x, texte_info["y"]))
                ecran.blit(texte, texte_rect)

            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    vrai = False

            #pour faire deffiler le texte :
            i = 0
            while i < len(textes):
                textes[i]["y"] += 2
                if textes[i]["y"] > 630 + 18:
                    textes.pop(i)
                else:
                    i += 1

            #si tous les textes ont été retirés le programme s'arrete
            if not textes:
                vrai = False

            clock.tick(10)

        pygame.quit()
        sys.exit()
        
    while True:
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if bouton_rect.collidepoint(event.pos):
                    pygame.init()
                    # Couleurs

                    #paramètres de l'écran
                    ecran = pygame.display.set_mode((1100,550))
                    pygame.display.set_caption("Combat Mathématique")
                    fond1  = pygame.image.load("Ressources\8.png")
                    pythagore = pygame.image.load("Ressources\A1.png")

                    #police
                    police = pygame.font.Font("Ressources\police.ttf",30)
                    police1 = pygame.font.Font("Ressources\police.ttf", 50)
                    

                    #ffonction pour afficher le texte
                    def afficher_texte(texte, police,couleur,surface,a,b):
                        texteobj = police.render(texte,1,couleur)
                        texterect = texteobj.get_rect()
                        texterect.topleft = (a,b)
                        surface.blit(texteobj, texterect)

                    # Fonction pour générer un calcul aléatoire
                    def generate_question():
                        num1 = random.randint(1, 30)
                        num2 = random.randint(1, 30)
                        operateur = random.choice(['+', '-', '*'])
                        question = f"{num1} {operateur} {num2}"
                        reponse = eval(question)
                        return question, reponse

                    #fonction principale du jeu
                    def main():
                        pts = 350
                        pts_int = pts
                        pts_finaux = 0
                        pv_joueur = 100
                        pv_pythagore = 100
                        clock = pygame.time.Clock()
                        question, reponse = generate_question()
                        temps_limite = 350  # Temps en milisecondes pour répondre à une question =35 secondes
                        Temps = temps_limite
                        temps2= Temps / 10
                        ajout = ''
                        
                        ecran.blit(fond1,(0,0))
                        ecran.blit(pythagore,(-100,-250))
                        afficher_texte("VIENS ME DEFIER", police1, blanc, ecran, 550, 300)
                        pygame.display.update()
                        time.sleep(3)

                        # pour que le jeu se répete 
                        while True:
                            
                            ecran.blit(fond1,(0,0))

                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    pygame.quit()
                                    sys.exit()

                                if event.type == pygame.KEYDOWN:
                                    if event.key == pygame.K_RETURN:
                                        #vérification de la réponse du joueur
                                        if ajout == str(reponse):
                                            pts_int = pts
                                            pv_pythagore -= 10
                                            pts_int = pts - Temps
                                            pts_finaux += pts_int
                                            question, reponse = generate_question()
                                            Temps = temps_limite
                                            temps2= Temps / 10
                                        else:
                                            pv_joueur -= 10

                                        #éfface de la réponse précédente
                                        ajout = ''

                                    elif event.key == pygame.K_BACKSPACE:
                                        ajout = ajout[:-1]
                                    else:
                                        ajout += event.unicode

                            #affichage des informations
                            afficher_texte(f"Pythagore: {pv_pythagore} pv", police, noir, ecran, 750, 10)
                            afficher_texte(f"Joueur: {pv_joueur} pv", police, noir, ecran, 10, 10)
                            afficher_texte(f"Temps restant: {temps2} s", police, noir, ecran, 325, 10)
                            afficher_texte("Répondez à la question:", police, noir, ecran, 10,50)
                            afficher_texte(question, police1, blanc, ecran, 10, 150)
                            afficher_texte(ajout, police, blanc, ecran, 30, 230)

                            #affichage de la victoire/défaite
                            if pv_pythagore <= 0:
                                ecran.fill(blanc)
                                afficher_texte("Vous avez gagné !",police1, noir, ecran,250,250)
                                pygame.display.update()
                                time.sleep(4)
                                ecran_titre(noir,bleu,police,pts_finaux)
                                pygame.quit()
                            elif pv_joueur <= 0:
                                ecran.fill(noir)
                                afficher_texte("Vous avez perdu !",police1, rouge, ecran,250,250)
                                pygame.display.update()
                                time.sleep(4)
                                ecran_titre(noir,blanc,police,pts_finaux)
                                pygame.quit()

                            #temps
                            if Temps > 0:
                                Temps -= 1
                                temps2= Temps / 10
                            else:
                                pv_joueur -= 10
                                question, reponse = generate_question()
                                Temps = temps_limite

                            pygame.display.flip()
                            clock.tick(30)
                            

                    if __name__ == "__main__":
                        main()


        fond3  = pygame.image.load("Ressources\Fond1.png").convert()
        ecran.blit(fond3,(0,0))
        pygame.draw.rect(ecran, blanc, bouton_rect)
        ecran.blit(texte_accueil, position_texte_accueil)
        ecran.blit(texte_instructions, position_texte_instructions)
        ecran.blit(texte_bouton, position_texte_bouton)


        
        pygame.display.flip()