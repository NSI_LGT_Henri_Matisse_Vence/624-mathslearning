 Pour démarrer MathsLearning il vous faudra :

- Ouvrir Thonny

- Appuyer sur 'Fichier', puis 'Ouvrir', puis sélectionner le fichier 'LaunchScreen.py' dans le dossier 'sources'

- Lancer le programme

- Choisir le jeu auquel vous voulez jouer et amusez vous !

--------------------
| !!!! WARNING !!!! |
--------------------
Power Games n'est pas disponible à cause d'un bug lors du lacement depuis l'écran d'accueil.
Cependant, nous nevoulons pas vous priver de ce divertissement !
Ainsi, afin de le tester, vous pourrez ouvrir le fichier 'Power_Games.py' sur Thonny comme à l'étape 2 et lancer le programme